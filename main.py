import cv2 as cv
import argparse
import sys
import numpy as np
import os.path

conf_threshold = 0.5
nms_threshold = 0.6  # Non-maximum suppression threshold
net_W, net_H = 416, 416

parser = argparse.ArgumentParser()
parser.add_argument('--video', help='Path to video file.')
args = parser.parse_args()

classes = None
with open("coco.names", 'rt') as f:
    classes = f.read().rstrip('\n').split('\n')

model_config = "./models/yolov3.cfg"
model_weights = "./models/yolov3.weights"

net = cv.dnn.readNetFromDarknet(model_config, model_weights)
net.setPreferableBackend(cv.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv.dnn.DNN_TARGET_CPU)


def get_outputs_names(net):
    layersNames = net.getLayerNames()
    return [layersNames[i[0] - 1] for i in net.getUnconnectedOutLayers()]


def draw_pred(classId, conf, left, top, right, bottom):
    cv.rectangle(frame, (left, top), (right, bottom), (255, 0, 0), 3)
    label = '%.2f' % conf
    if classes:
        assert (classId < len(classes))
        label = '%s:%s' % (classes[classId], label)
    (label_width, label_height), baseline = cv.getTextSize(label, cv.FONT_HERSHEY_SIMPLEX, 0.75, 1) #Calculates the width and height of a text string.
    top = max(top, label_height)
    cv.rectangle(frame, (left, top - round(1.5 * label_height)), (left + round(1.5 * label_width), top + baseline),
                 (255, 255, 255), cv.FILLED)
    cv.putText(frame, label, (left, top), cv.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 0), 1)


def postprocess(frame, outs):
    frame_height = frame.shape[0]
    frame_width = frame.shape[1]
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > conf_threshold:
                center_x = int(detection[0] * frame_width)
                center_y = int(detection[1] * frame_height)
                width = int(detection[2] * frame_width)
                height = int(detection[3] * frame_height)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                class_ids.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

    indices = cv.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        draw_pred(class_ids[i], confidences[i], left, top, left + width, top + height)



if (args.video):
    if not os.path.isfile(args.video):
        print("Input video file doesn't exist")
        sys.exit(1)
    cap = cv.VideoCapture(args.video)
    output_file = args.video[:-4] + '_yolo_out.avi' ##assuming its .avi

vid_writer = cv.VideoWriter(output_file, cv.VideoWriter_fourcc('M', 'J', 'P', 'G'), 30, (round(cap.get(3)), round(cap.get(4))))

while not cv.waitKey(1) > 0:
    ret, frame = cap.read()
    if not ret:
        print("Output file is stored as ", output_file)
        cap.release()
        break
    blob = cv.dnn.blobFromImage(frame, 1 / 255, (net_W, net_H), [0, 0, 0], 1, crop=False)
    net.setInput(blob)
    outs = net.forward(get_outputs_names(net))
    postprocess(frame, outs)
    vid_writer.write(frame.astype(np.uint8))
    cv.imshow('window', frame)
